<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $table = 'news';
    protected $fillable=['id_user', 'title', 'description'];

    public function relUser(){
        return $this->hasOne('App\Models\User', 'id', 'id_user');
    }

}
 