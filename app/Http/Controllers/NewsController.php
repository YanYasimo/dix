<?php

namespace App\Http\Controllers;


use App\Http\Requests\NewsRequest;
use App\Http\Requests\Request;
use App\Models\News;
use App\Models\User;
use Auth;
use DB;
use RealRashid\SweetAlert\Facades\Alert;

class NewsController extends Controller
{

    private $objUser;
    private $objNews;

    public function __construct(){
        $this->objUser = new User();
        $this->objNews = new News();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::check())
        {
            $id = Auth::user()->getId();
        }

        //$news = $this->objNews->all()->sortByDesc('id');
        $news = $this->objNews->where('id_user', $id)->orderBy('id', 'desc')->paginate(10);
        return view('pages.news', compact('news'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(NewsRequest $request)
    {
        if (Auth::check())
        {
            $id = Auth::user()->getId();
        }

        $new=$this->objNews->create([
            'title'=> $request->title,
            'description'=> $request->description,
            'id_user' => $id
        ]);
        if($new){
            Alert::success('Successo', 'Notícia criada com êxito');
            return redirect('news');
        }
        Alert::error('Erro', 'Notícia não foi criada');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response 
     */
    public function show($id)
    {
       $news=$this->objNews->find($id);
       return view('show', compact('news'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $news = $this->objNews -> find($id);
        return view('create', compact('news'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(NewsRequest $request, $id)
    {
        $update=$this->objNews->where(['id'=>$id])->update([
            'title'=> $request->title,
            'description'=> $request->description
        ]);
        if($update){
            Alert::success('Sucesso', 'A notícia foi atualizada');
            return redirect('news');
        }
        Alert::error('Erro', 'Atualização de notícia não efetuada');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $destroy = $this->objNews -> find($id)->delete();
        if($destroy){
            Alert::success('Sucesso', 'A notícia foi excluída');
            return redirect('news');
        }
        Alert::error('Erro', 'Exclusão não efetuada');
    }

    /**
     * Procurar notícias por título
     */
    public function search(\Illuminate\Http\Request $request){
        $filter = $request->filter;
        
        $news = $this->objNews->where(function($query) use ($filter){
            if($filter) {
                if (Auth::check())
                {
                    $id = Auth::user()->getId();
                }
        
                $query->where('title', 'LIKE', "%{$filter}%")
                        ->where('id_user', $id)
                        ->orderBy('id', 'desc');
            }
            else{
                if (Auth::check())
                {
                    $id = Auth::user()->getId();
                }
        
                $query->where('id_user', $id)
                    ->orderBy('id', 'desc');
            }
        })->orderBy('id', 'desc')->paginate(30);


        return view('pages.news', compact('news'));
    }
}
