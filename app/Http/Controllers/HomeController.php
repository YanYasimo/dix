<?php

namespace App\Http\Controllers;
use App\Models\User;
use App\Models\News;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->objUser = new User();
        $this->objNews = new News();
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {        
        if (Auth::check())
        {
            $id = Auth::user()->getId();
        }

        $user = $this->objUser->all()->where('id', $id);
        $news = $this->objNews->where('id_user', $id)->orderBy('id', 'desc')->paginate(10);
        //dd($user);
        return view('dashboard', compact(['user','news']));
    }
}
