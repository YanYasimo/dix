@extends('layouts.app', ['pageSlug' => 'show'])

@section('content')
    <div class="col-12 m-auto">
        
    <h1 class="title" style="font-size:50px"> {{ $news->title }} </h1>
        @php 
            $user=$news->find($news->id)->relUser;
        @endphp

        <div class="publisher" style="
            font-size:12px;
            text-align:justify;
            padding-left:90px;
            padding-bottom:50px;
            padding-top:40px;">
            <p>Por {{ $user->name }} | </p>
            <p>{{ $news->created_at }}</p>
        </div>

        <hr>

        <img src="/img/extra-1200x630.jpg" style="
            width:50%;  
            display: block;
            margin-left: auto;
            margin-right: auto;
            margin-top:50px;
            width: 40%;
            border-radius: 7px">



        <div style="
            font-size:20px;
            width:100%;
            text-align:justify;
            padding:150px;
            padding-top:100px;
            display:flex">
            <p >
              {{ $news->description }}                
                <br>
                <br>
            </p>
    </div>
@endsection