@extends('layouts.app', ['page' => __('News'), 'pageSlug' => 'news'])


@section('content')
      <div>
        <h1 class="title">Gerenciar Notícias</h1>
      </div>
    <div class="card">
        <div class="card-header">
            <div style="float:left">
                <form class="search form form-inline" method="post" action="/news/search">
                    {{csrf_field()}} 
                    <input type="text" placeholder="Pesquisar por título" class="form-control" name="filter"/>
                    <button type="submit" class="btn btn-info btn-sm btn-icon"><i class="tim-icons icon-zoom-split"></i></button>
                </form>
            </div>
            <div style="float:right">
                <a class="btn-group-sm btn-group-toggle float-right" href="{{ url("news/create") }}">
                    <label class="btn btn-sm btn-primary btn-simple active" id="0">
                        <span class="d-none d-sm-block d-md-block d-lg-block d-xl-block">Criar</span>
                        <span class="d-block d-sm-none">
                        </span>
                    </label>
                </a>
            </div>
        </div>
        <hr>
            @foreach($news as $newses)
        <div class="card-body" style="padding-top:10px">
                    <div style="float:left">
                        <a href="{{ url("news/$newses->id") }}">
                            <h4 class="card-title">{{$newses->title}}</h4>
                            <p class="card-text">{{$newses->created_at}}</p>
                        </a>
                    </div>
                    <div style="float:right">
                        <div style="float:left">
                            <a href="{{ url("news/{$newses->id}/edit") }}"> 
                                <button type="button" rel="tooltip" class="btn btn-success btn-sm btn-icon">
                                    <i class="tim-icons icon-settings"></i>
                                </button>
                            </a>
                        </div>
                        <div style="float:right">
                            <form class="delete" method="post" action="{{ url("news/{$newses->id}/delete") }}" >
                                @csrf 
                                @method('DELETE')
                                <button type="submit" rel="tooltip" class="btn btn-danger btn-sm btn-icon remove-user" onclick="return myFunction();">
                                    <i class="tim-icons icon-simple-remove"></i>
                                </button>
                            </form>
                        </div>
                    </div>
                    
        </div>
        <hr>
        @endforeach
        <div class="card-footer">
            {{$news->links()}} 
        </div>
    </div>
@endsection