@extends('layouts.app', ['pageSlug' => 'dashboard'])

@section('content')
    <h1> @if(isset($news)) Editar Notícia @else Cadastrar Notícia @endif </h1>
    <div class="col-12 m-auto">
        @if(isset($errors) && count($errors)>0)
            <div class="text-center m-auto alert alert-danger" role="alert">
                <h4>Alert!</h4>
                @foreach($errors->all() as $error)
                    {{$error}}<br>
                @endforeach
            </div>
        @endif



        <div class="card">
            <div class="card-body">
                @if(isset($news)) 
                    <form name="formEditNews" id="formEditNews" method="post" action="{{ url("news/{$news->id}") }}">   
                    @method('PUT')         
                @else  
                    <form name="formNews" id="formNews" method="post" action="{{ url('news') }}">
                @endif

                    @csrf
                    
                    <div class="form-group">
                        <label for="exampleFormControlInput1">Título</label>
                        <input type="text" name="title" class="form-control" id="title" placeholder="Título"  required value="{{ $news->title ?? ''}}">
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlTextarea1">Descrição</label>
                        <textarea type="text" name="description" rows="15" class="form-control" id="description" placeholder="Descrição"  required style="height:auto">{{$news->description ?? ''}}</textarea>
                    </div>
                    <div class="text-center mt-3 mb-4">
                        <input class="btn btn-primary btn-round btn-lg text-center" type="submit" value="@if(isset($news)) Editar @else Cadastrar @endif" />
                    </div>
                    
                </form>
            </div>
        </div>
    </div>
@endsection