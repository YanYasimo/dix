@extends('layouts.app', ['page' => __('News'), 'pageSlug' => 'news'])


@section('content')
      <div>
        <h1 class="title">Gerenciar Notícias</h5>
      </div>
    <div class="text-center mt-3 mb-4">
        
        <a href="{{ url("news/create") }}">
            <button class="btn btn-primary btn-round btn-lg">{{ _('Criar Notícia') }}</button>
        </a>

    </div>      
    <div class="card-header">
        <form class="search form form-inline" method="post" action="/news/search">
            {{csrf_field()}} 
            <input type="text" placeholder="Pesquisar por título" class="form-control" name="filter"/>
            <button type="submit" class="btn btn-info btn-sm btn-icon"><i class="tim-icons icon-zoom-split"></i></button>
        </form>
    </div>
    <div class="card-body">
    <div class="col-12 m-auto">
        @csrf
        <table class="table">
        <thead>
            <tr>
                <th class="text-center">ID</th>
                <th>Título</th>
                <th>Descrição</th>
                <th>Proprietário</th>
                <th>Criado em</th>
                <th>Atualizado em</th>
                <th class="row">

                </th>    
                
            </tr>
        </thead>
        <tbody>

        @foreach($news as $newses)
            @php
                $user=$newses->find($newses->id)->relUser;
            @endphp
            <tr>
                <td class="text-center"> {{ $newses->id }}</td>
                <td>{{ $newses->title }}</td>
                <td>                    
                    <div class="form-group">
                        <textarea name="description" class="form-control text-justified" id="description" rows="5" disabled >{{$newses->description}}</textarea>
                    </div>
                </td>
                <td>{{ $user->name }}</td>
                <td>{{ $newses->created_at }}</td>
                <td>{{ $newses->updated_at }}</td>
                <td class="td-actions text-right row pt-5">
                    <a href="{{ url("/news/{$newses->id}") }}">
                        <button type="button" rel="tooltip" class="btn btn-info btn-sm btn-icon">
                            <i class="tim-icons icon-single-02"></i>
                        </button>
                    </a>
                    <a href="{{ url("/news/{$newses->id}/edit") }}">
                        <button type="button" rel="tooltip" class="btn btn-success btn-sm btn-icon">
                            <i class="tim-icons icon-settings"></i>
                        </button>
                    </a>
                    <form class="delete" method="post" action="{{ url("news/{$newses->id}/delete") }}" >
                        @csrf 
                        @method('DELETE')
                        <button type="submit" rel="tooltip" class="btn btn-danger btn-sm btn-icon remove-user" onclick="return myFunction();">
                            <i class="tim-icons icon-simple-remove"></i>
                        </button>
                    </form>
                </td>
            </tr>
            
        @endforeach
        <script type="text/javascript">
          function myFunction() {
            if(!confirm("Atenção! Deseja realmente excluir?"))
            event.preventDefault();
            }
        </script>
        </tbody>
    </table>
    {{$news->links()}}
    </div>
</div>
@endsection