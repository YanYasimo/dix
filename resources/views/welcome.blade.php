@extends('layouts.app', ['pageSlug' => 'dashboard'])

@section('content')
    <div class="header py-7 py-lg-8">
        <div class="container">
            <div class="header-body text-center mb-7">
                <div class="row justify-content-center">
                    <div class="col-lg-5 col-md-6">
                        <div class="cardheader">
                            <h1 class="text-lead text-strong">Bem Vindo</h1>
                            <p class="text-lead text-strong" style="font-size:30px">
                                {{ __('Faça login ou registre-se') }}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
