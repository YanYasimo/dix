@extends('layouts.app', ['class' => 'register-page', 'page' => _('Register Page'), 'contentClass' => 'register-page'])


@section('content')

<div class="wrapper">
    <div class="sidebar">
        <div class="sidebar-wrapper">
            <div class="logo">
                <a href="#" class="simple-text logo-mini">{{ _('WD') }}</a>
                <a href="#" class="simple-text logo-normal">{{ _('White Dashboard') }}</a>
            </div>
            <ul class="nav">
                <li>
                    <a href="{{ route('home') }}">
                        <i class="tim-icons icon-chart-pie-36"></i>
                        <p>{{ _('Dashboard') }}</p>
                    </a>
                </li>

                <li>
                    <a href="{{ route('pages.news') }}">
                        <i class="tim-icons icon-pencil"></i>
                        <p>{{ _('Gerenciar Suas Notícias') }}</p>
                    </a>
                </li>

                <li>
                    <a data-toggle="collapse" href="#laravel-examples" aria-expanded="true">
                        <i class="tim-icons icon-badge"></i>
                        <span class="nav-link-text">{{ __('Opções de usuário') }}</span>
                        <b class="caret mt-1"></b>
                    </a>

                    <div class="collapse show" id="laravel-examples">
                        <ul class="nav pl-4">
                            <li>
                                <a href="{{ route('profile.edit')  }}">
                                    <i class="tim-icons icon-single-02"></i>
                                    <p>{{ _('Perfil') }}</p>
                                </a>
                            </li>
                            <li class="active">
                                <a href="{{ route('user.index')  }}">
                                    <i class="tim-icons icon-bullet-list-67"></i>
                                    <p>{{ _('Gerenciamento') }}</p>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <!--  $pageSlug == 'upgrade' ? 'active' : ''   -->
                <li class=" {{ $pageSlug = '' }} bg-info">
                    <a href="{{ route('pages.upgrade') }}">
                        <i class="tim-icons icon-spaceship"></i>
                        <p>{{ _('Upgrade to PRO') }}</p>
                    </a>
                </li>
            </ul>
        </div>
    </div>

    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card ">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-8">
                                <h2 class="card-title">Usuários</h2>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="">
                            <table class="table tablesorter " id="">
                                <thead class=" text-primary">
                                    <tr>
                                        <th scope="col">Nome</th>
                                        <th scope="col">Email</th>
                                        <th scope="col">Criado em</th>
                                        <th scope="col"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($users as $user)
                                    <tr>
                                        <td>{{$user->name}}</td>
                                        <td>
                                            <a href="mailto:{{$user->email}}">{{$user->email}}</a>
                                        </td>
                                        <td>{{$user->created_at}}</td>
                                        @php 
                                            if (Auth::check()) $id = Auth::user()->getId();
                                        @endphp

                                        @if($user->id == $id)
                                            <td class="text-right">
                                                <div class="dropdown">
                                                    <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <i class="fas fa-ellipsis-v"></i>
                                                    </a>
                                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                        <a class="dropdown-item" href="{{ route('profile.edit')  }}">Editar</a> 
                                                    </div>
                                                </div>

                                            </td>

                                        @endif

                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<form id="logout-form" action="{{route('logout')}}" method="POST" style="display: none;">
    @csrf
    <div class="fixed-plugin">
        <div class="dropdown show-dropdown">
            <a href="#" data-toggle="dropdown">
                <i class="fa fa-cog fa-2x"> </i>
            </a>
            <ul class="dropdown-menu">
                <li class="header-title"> Sidebar Background</li>
                <li class="adjustments-line">
                    <a href="javascript:void(0)" class="switch-trigger background-color">
                        <div class="badge-colors text-center">
                            <span class="badge filter badge-primary active" data-color="primary"></span>
                            <span class="badge filter badge-info" data-color="blue"></span>
                            <span class="badge filter badge-success" data-color="green"></span>
                        </div>
                        <div class="clearfix"></div>
                    </a>
                </li>
                <li class="button-container">
                    <a href="https://www.creative-tim.com/product/white-dashboard-laravel" target="_blank" class="btn btn-primary btn-block btn-round">Download Now</a>
                    <a href="https://white-dashboard-laravel.creative-tim.com/docs/getting-started/introduction.html" target="_blank" class="btn btn-default btn-block btn-round">
                        Documentation
                    </a>
                </li>
                <li class="header-title">Thank you for 95 shares!</li>
                <li class="button-container text-center">
                    <button id="twitter" class="btn btn-round btn-info"><i class="fab fa-twitter"></i> · 45</button>
                    <button id="facebook" class="btn btn-round btn-info"><i class="fab fa-facebook-f"></i> · 50</button>
                    <br>
                    <br>
                    <a class="github-button btn btn-round btn-default" href="https://github.com/creativetimofficial/white-dashboard-laravel" data-icon="octicon-star" data-size="large" data-show-count="true" aria-label="Star ntkme/github-buttons on GitHub">Star</a>
                </li>
            </ul>
        </div>
    </div>
    @endsection