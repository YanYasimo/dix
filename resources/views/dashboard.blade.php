@extends('layouts.app', ['pageSlug' => 'dashboard'])

@section('content')
<div class="row">

    @foreach($user as $us)
    <div style="  
        width: 50%;
        padding: 10px;">

        <h1>Olá, Seja Bem-Vindo {{$us->name}}</h1>

    </div>
    @endforeach



    <div class="col-sm-8">
        <div class="card shadow p-3 mb-5 bg-white rounded">
            <div class="card-header">
                <div style="float:left">
                    <h4 class="card-title">Últimas postagens</h4>
                </div>
                <a class="btn-group btn-group-toggle float-right" href="{{ url("news/create") }}">
                    <label class="btn btn-sm btn-primary btn-simple active" id="0">
                        <span class="d-none d-sm-block d-md-block d-lg-block d-xl-block">Criar</span>
                        <span class="d-block d-sm-none">
                        </span>
                    </label>
                </a>
            </div>
            <hr>
            <div class="card-body">
                @foreach($news as $newses)
                <a href="{{ url("news/$newses->id") }}">
                    <h4 class="card-title">{{$newses->title}}</h4>
                    <p class="card-text">{{$newses->created_at}}</p>
                </a>
                <br>

                @endforeach

            </div>
            <div class="card-footer">
                <div class="d-flex justify-content-between">
                    <button type="submit" class="btn btn-primary btn-lg btn-block">{{ _('Ver mais notícias') }}</button>
                </div>
            </div>
        </div>
    </div>

    <div class="card m-b-20">
        <div class="card card shadow p-3 mb-5 bg-white rounded">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-sm-6 text-left">
                            <h5 class="card-category">Total Shipments</h5>
                            <h2 class="card-title">Performance</h2>
                        </div> 
                        <!--
                        <div class="col-sm-6">
                            <div class="btn-group btn-group-toggle float-right" data-toggle="buttons">
                                <label class="btn btn-sm btn-primary btn-simple active" id="0">
                                    <input type="radio" name="options" checked>
                                    <span class="d-none d-sm-block d-md-block d-lg-block d-xl-block">Accounts</span>
                                    <span class="d-block d-sm-none">
                                        <i class="tim-icons icon-single-02"></i>
                                    </span>
                                </label>
                                <label class="btn btn-sm btn-primary btn-simple" id="1">
                                    <input type="radio" class="d-none d-sm-none" name="options">
                                    <span class="d-none d-sm-block d-md-block d-lg-block d-xl-block">Purchases</span>
                                    <span class="d-block d-sm-none">
                                        <i class="tim-icons icon-gift-2"></i>
                                    </span>
                                </label>
                                <label class="btn btn-sm btn-primary btn-simple" id="2">
                                    <input type="radio" class="d-none" name="options">
                                    <span class="d-none d-sm-block d-md-block d-lg-block d-xl-block">Sessions</span>
                                    <span class="d-block d-sm-none">
                                        <i class="tim-icons icon-tap-02"></i>
                                    </span>
                                </label>
                            </div>
                        </div> -->
                    </div>
                </div>
                <div class="card-body">
                    <div class="chart-area">
                    <canvas id="myChart" style="display: block; width: 500px; height: 200px;" width="500" height="200" class="chartjs-render-monitor"></canvas>
                    </div>
                </div>
                <div class="card-footer">

                </div>
            </div>
        </div>
    </div>


</div>
@endsection

@section('scripts')
<script src="{{ asset('chart/Chart.js') }}"></script>
<script>
var ctx = document.getElementById('myChart').getContext('2d');

var myChart = new Chart(ctx, {
    type: 'line',
    data: {
        labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
        datasets: [{
            label: 'Quantidade de Pesquisas',
            data: [1, 59, 3, 5, 20 , 2],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                display:true,
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});
</script>
@endsection

@push('js')
<script src="{{ asset('white') }}/js/plugins/chartjs.min.js"></script>
<script>
    $(document).ready(function() {
        demo.initDashboardPageCharts();
    });
</script>
@endpush